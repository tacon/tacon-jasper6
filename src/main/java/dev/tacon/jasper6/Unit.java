package dev.tacon.jasper6;

import dev.tacon.annotations.NonNull;

/**
 * Enum to represent various units of length.
 * Provides utility methods for unit conversion.
 */
public enum Unit {

	/**
	 * Unit representing Meters.
	 */
	METER(1.),
	/**
	 * Unit representing Centimeters.
	 */
	CENTIMETER(0.01),
	/**
	 * Unit representing Inches.
	 */
	INCH(0.0254),
	/**
	 * Unit representing Points.
	 */
	POINT(0.0254 / 72.);

	/**
	 * The conversion factor to meters for this unit.
	 */
	private final double thisOverMeter;

	/**
	 * Constructor for a Unit enum member.
	 * 
	 * @param thisOverMeter The conversion factor to meters for this unit.
	 */
	Unit(final double thisOverMeter) {
		this.thisOverMeter = thisOverMeter;
	}

	/**
	 * Converts a value from this unit to another unit.
	 * 
	 * @param otherUnit The unit to convert to.
	 * @param value The value in this unit.
	 * @return The converted value in the target unit.
	 */
	public double toUnit(final @NonNull Unit otherUnit, final double value) {
		return value * this.thisOverMeter / otherUnit.thisOverMeter;
	}

	/**
	 * Static utility method to convert values between different units.
	 * 
	 * @param sourceUnit The unit of the input value.
	 * @param resultUnit The unit to convert to.
	 * @param value The value to convert.
	 * @return The converted value in the target unit.
	 */
	public static double convert(final @NonNull Unit sourceUnit, final @NonNull Unit resultUnit, final double value) {
		return sourceUnit.toUnit(resultUnit, value);
	}
}